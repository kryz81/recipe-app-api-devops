terraform {
  backend "s3" {
    bucket         = "kryz-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    encrypt        = true
    region         = "us-east-1"
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
}
